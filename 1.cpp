#include <conio.h>
#include <graphics.h>
#include<stdio.h>

#define High 480  // 游戏画面尺寸
#define Width 640

// 全局变量
int ball_x,ball_y; // 小球的坐标
int ball_vx,ball_vy; // 小球的速度
int radius;  // 小球的半径
int position_x,position_y,R;
int left,right;
int up,low;

void startup()  // 数据初始化
{
	ball_x = Width/2;
	ball_y = High/2;
	ball_vx = 1;
	ball_vy = 1;
	radius = 20;
	position_x=Width/2;
	position_y=High-20;
	R=100;
	left=position_x-R;
	right=position_x+R;
	up=position_y-20;
	low=position_y+20;
	initgraph(Width, High);
	BeginBatchDraw();
}

void clean()  // 显示画面
{
	// 绘制黑线、黑色填充的圆
	setcolor(BLACK);
	setfillcolor(BLACK);
	fillcircle(ball_x, ball_y, radius);
	solidrectangle(left,up,right,low);
	
}	

void show()  // 显示画面
{
	// 绘制黄线、绿色填充的圆
	setcolor(YELLOW);
	setfillcolor(GREEN);
	fillcircle(ball_x, ball_y, radius);	
	solidrectangle(left,up,right,low);
	FlushBatchDraw();
	// 延时
	Sleep(3);	
}	

void updateWithoutInput()  // 与用户输入无关的更新
{
	// 更新小圆坐标
	ball_x = ball_x + ball_vx;
	ball_y = ball_y + ball_vy;
	//
	left=position_x-R;
	right=position_x+R;
	up=position_y-20;
	low=position_y+20;
	if ((ball_x<=radius)||(ball_x>=Width-radius))
		ball_vx = -ball_vx;
	if ((ball_y<=radius)||(ball_y>=High-radius))
		ball_vy = -ball_vy;	
}

void updateWithInput()  // 与用户输入有关的更新
{	
	char input;
	if(kbhit()){
		input=getch();

		if(input=='a' && left>=0)
			position_x--;
		if(input=='d' && right<=Width)
			position_x++;
		if(input=='w' && position_y-R>=0)
			position_y--;
		if(input=='s' && position_y+R<=High)
			position_y++;
	}
}

void gameover()
{
	EndBatchDraw();
	closegraph();
}

int main()
{
	startup();  // 数据初始化	
	while (1)  //  游戏循环执行
	{
		clean();  // 把之前绘制的内容清除
		updateWithoutInput();  // 与用户输入无关的更新
		updateWithInput();     // 与用户输入有关的更新
		show();  // 显示新画面
	}
	gameover();     // 游戏结束、后续处理
	return 0;
}